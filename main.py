from PyQt5 import QtCore, QtGui, QtWidgets
from additionalWidget import mainSearchWidget, selectDocuments, simpleOptionWidget, getMainApp, localSearchDialog, customTableWidgetItem
from constants import GENERAL_FONT_NAME, TEXTEDIT_FONT, OPTION_HEIGHT, BORDER_COLOR, BUTTON_BACKGROUND_COLOR, BUTTON_HOVER_BACKGROUND_COLOR, INITIAL_ALIGN, LOCAL_SEARCH_HIGHLIGHT_COLOR, SELECTED_SEARCH_HIGHLIGHT_COLOR, GLOBAL_SEARCH_HIGHLIGHT_COLOR
import sys
import shutil
import os
import json
from tika import tika
from tika import parser
import tempfile
import whoosh_class


class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None) -> None:
        super().__init__(parent=parent)
        self.whoosh = whoosh_class.whooshIndexer()
        self.previewFilePath = None
        self.isLocalSearchEctivated = False
        getMainApp(app)

        self.resize(1400, 800)
        self.setWindowTitle('متن یاب')
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")

        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setContentsMargins(20, 20, 20, 20)
        self.horizontalLayout.setSpacing(10)
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.leftFrame = QtWidgets.QFrame(self.centralwidget)
        self.leftFrame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.leftFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.leftFrame.setStyleSheet("#leftFrame{border: 3px solid %s; border-radius: 14px; background-color: #FFFFFF}" %(BORDER_COLOR))
        self.leftFrame.setObjectName("leftFrame")

        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.leftFrame)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 10)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")

        self.fileFrame = QtWidgets.QFrame(self.leftFrame)
        self.fileFrame.setFixedHeight(OPTION_HEIGHT+10)
        self.fileFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.fileFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fileFrame.setObjectName("fileFrame")
        self.verticalLayout_3.addWidget(self.fileFrame)

        self.line = QtWidgets.QFrame(self.leftFrame)
        self.line.setMinimumSize(QtCore.QSize(0, 3))
        self.line.setStyleSheet("background-color: %s;" %(BORDER_COLOR))
        self.line.setLineWidth(0)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.verticalLayout_3.addWidget(self.line)

        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.fileFrame)
        self.horizontalLayout_3.setContentsMargins(0, 0, 20, 0)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")

        self.operatorsFrame = QtWidgets.QFrame(self.fileFrame)
        self.operatorsFrame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.operatorsFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.operatorsFrame.setStyleSheet("""QPushButton{border: none;
                                                         padding: 8px;
                                                         border-radius: 5px}
                                             QPushButton:hover{background-color: %s;
                                             }""" %(BUTTON_BACKGROUND_COLOR))
        self.operatorsFrame.setObjectName("operatorsFrame")

        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.operatorsFrame)
        self.horizontalLayout_4.setSpacing(4)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")

        self.localSearchButton = QtWidgets.QPushButton(self.operatorsFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.localSearchButton.sizePolicy().hasHeightForWidth())
        self.localSearchButton.setSizePolicy(sizePolicy)
        self.localSearchButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("image/search.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.localSearchButton.setIcon(icon)
        self.localSearchButton.setIconSize(QtCore.QSize(24, 24))
        self.localSearchButton.setObjectName("localSearchButton")
        self.localSearchButton.clicked.connect(self.showLocalSearchWindow)
        self.localSearchButton.setEnabled(False)
        self.horizontalLayout_4.addWidget(self.localSearchButton)

        self.previousButton = QtWidgets.QPushButton(self.operatorsFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.previousButton.sizePolicy().hasHeightForWidth())
        self.previousButton.setSizePolicy(sizePolicy)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("image/upArrow.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.previousButton.setIcon(icon1)
        self.previousButton.setIconSize(QtCore.QSize(24, 24))
        self.previousButton.setObjectName("previousButton")
        self.previousButton.setDisabled(True)
        self.previousButton.clicked.connect(self.F_GlobalSearchPreviousItem)
        self.horizontalLayout_4.addWidget(self.previousButton)

        self.nextButton = QtWidgets.QPushButton(self.operatorsFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.nextButton.sizePolicy().hasHeightForWidth())
        self.nextButton.setSizePolicy(sizePolicy)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("image/downArrow.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.nextButton.setIcon(icon2)
        self.nextButton.setIconSize(QtCore.QSize(24, 24))
        self.nextButton.setObjectName("nextButton")
        self.nextButton.setDisabled(True)
        self.nextButton.clicked.connect(self.F_GlobalSearchNextItem)
        self.horizontalLayout_4.addWidget(self.nextButton)

        self.counterLabel = QtWidgets.QLabel(self.operatorsFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.counterLabel.sizePolicy().hasHeightForWidth())
        self.counterLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont(GENERAL_FONT_NAME, 12)
        self.counterLabel.setFont(font)
        self.counterLabel.setObjectName("counterLabel")
        self.counterLabel.setText("0/0")
        self.counterLabel.setStyleSheet("padding: 0px 10px 0px 0px")
        self.horizontalLayout_4.addWidget(self.counterLabel)

        self.alignLeftButton = QtWidgets.QPushButton(self.operatorsFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.alignLeftButton.sizePolicy().hasHeightForWidth())
        self.alignLeftButton.setSizePolicy(sizePolicy)
        self.alignLeftButton.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("image/alignLeft.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.alignLeftButton.setIcon(icon3)
        self.alignLeftButton.setIconSize(QtCore.QSize(24, 24))
        self.alignLeftButton.setObjectName("alignLeftButton")
        self.alignLeftButton.clicked.connect(self.F_AlignLeft)
        self.horizontalLayout_4.addWidget(self.alignLeftButton)

        self.alignCenterButton = QtWidgets.QPushButton(self.operatorsFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.alignCenterButton.sizePolicy().hasHeightForWidth())
        self.alignCenterButton.setSizePolicy(sizePolicy)
        self.alignCenterButton.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("image/alignCenter.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.alignCenterButton.setIcon(icon4)
        self.alignCenterButton.setIconSize(QtCore.QSize(24, 24))
        self.alignCenterButton.setObjectName("alignCenterButton")
        self.alignCenterButton.clicked.connect(self.F_AlignCenter)
        self.horizontalLayout_4.addWidget(self.alignCenterButton)

        self.alignRightButton = QtWidgets.QPushButton(self.operatorsFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.alignRightButton.sizePolicy().hasHeightForWidth())
        self.alignRightButton.setSizePolicy(sizePolicy)
        self.alignRightButton.setText("")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap("image/alignRight.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.alignRightButton.setIcon(icon5)
        self.alignRightButton.setIconSize(QtCore.QSize(24, 24))
        self.alignRightButton.setObjectName("alignRightButton")
        self.alignRightButton.clicked.connect(self.F_AlignRight)
        self.horizontalLayout_4.addWidget(self.alignRightButton)
        self.horizontalLayout_3.addWidget(self.operatorsFrame, 0, QtCore.Qt.AlignLeft)

        self.fileNameLabel = QtWidgets.QLabel(self.fileFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fileNameLabel.sizePolicy().hasHeightForWidth())
        self.fileNameLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont(GENERAL_FONT_NAME, 12)
        self.fileNameLabel.setFont(font)
        self.fileNameLabel.setObjectName("fileNameLabel")
        self.fileNameLabel.setText("نام فایل")
        self.horizontalLayout_3.addWidget(self.fileNameLabel)

        self.textEdit = QtWidgets.QTextEdit(self.leftFrame)
        self.textEditAlign = None
        self.textEdit.setFont(QtGui.QFont(TEXTEDIT_FONT[0], TEXTEDIT_FONT[1]))
        self.textEdit.setUndoRedoEnabled(False)
        self.textEdit.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.textEdit.setReadOnly(True)
        self.textEdit.setTextInteractionFlags(self.textEdit.textInteractionFlags() | QtCore.Qt.TextSelectableByMouse | QtCore.Qt.TextSelectableByKeyboard)

        if INITIAL_ALIGN == 'right':
            self.F_AlignRight()
        elif INITIAL_ALIGN == 'left':
            self.F_AlignLeft()
        elif INITIAL_ALIGN == 'center':
            self.F_AlignCenter()

        self.textEdit.setObjectName("textEdit")
        self.verticalLayout_3.addWidget(self.textEdit)
        self.horizontalLayout.addWidget(self.leftFrame)

        self.middleFrame = QtWidgets.QFrame(self.centralwidget)
        self.middleFrame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.middleFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.middleFrame.setObjectName("middleFrame")

        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.middleFrame)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setSpacing(10)
        self.verticalLayout_4.setObjectName("verticalLayout_4")

        self.middleContainerFrame = QtWidgets.QFrame(self.middleFrame)
        self.middleContainerFrame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.middleFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.middleContainerFrame.setObjectName("middleContainerFrame")
        self.middleContainerFrame.setStyleSheet("#middleContainerFrame{border: 3px solid %s; border-radius: 14px; background-color: #FFFFFF}" %(BORDER_COLOR))
        self.verticalLayout_4.addWidget(self.middleContainerFrame)

        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.middleContainerFrame)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 10)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")

        self.resultFrame = QtWidgets.QFrame(self.middleContainerFrame)
        self.resultFrame.setFixedHeight(OPTION_HEIGHT+10)
        self.resultFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.resultFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.resultFrame.setObjectName("resultFrame")

        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.resultFrame)
        self.horizontalLayout_2.setContentsMargins(20, 0, 20, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")

        self.casesNumberLabel = QtWidgets.QLabel(self.resultFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.casesNumberLabel.sizePolicy().hasHeightForWidth())
        self.casesNumberLabel.setSizePolicy(sizePolicy)
        self.casesNumberLabel.setObjectName("casesNumberLabel")
        self.casesNumberLabel.setFont(font)
        self.casesNumberLabel.setText("0 مورد")
        self.horizontalLayout_2.addWidget(self.casesNumberLabel, 0, QtCore.Qt.AlignLeft)

        self.resultLabel = QtWidgets.QLabel(self.resultFrame)
        self.resultLabel.setObjectName("resultLabel")
        self.resultLabel.setFont(font)
        self.resultLabel.setText("نتایج")
        self.horizontalLayout_2.addWidget(self.resultLabel, 0, QtCore.Qt.AlignRight)
        self.verticalLayout_2.addWidget(self.resultFrame)

        self.line2 = QtWidgets.QFrame(self.middleContainerFrame)
        self.line2.setMinimumSize(QtCore.QSize(0, 3))
        self.line2.setStyleSheet("background-color: %s;" %(BORDER_COLOR))
        self.line2.setLineWidth(0)
        self.line2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.verticalLayout_2.addWidget(self.line2)

        self.resultTabel = QtWidgets.QTableWidget(self.middleContainerFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.resultTabel.sizePolicy().hasHeightForWidth())
        self.resultTabel.setSizePolicy(sizePolicy)
        self.resultTabel.setSortingEnabled(True)
        # self.resultTabel.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.resultTabel.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.resultTabel.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.resultTabel.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.resultTabel.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.resultTabel.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.resultTabel.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.resultTabel.setShowGrid(True)
        self.resultTabel.setWordWrap(True)
        # self.resultTabel.setRowCount(10)
        self.resultTabel.setColumnCount(4)
        
        self.resultTabel.setObjectName("resultTabel")
        item = QtWidgets.QTableWidgetItem("عنوان")
        self.resultTabel.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem("امتیاز")
        self.resultTabel.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem("اندازه")
        self.resultTabel.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem("نام فایل")
        self.resultTabel.setHorizontalHeaderItem(0, item)
        self.resultTabel.horizontalHeader().setCascadingSectionResizes(True)
        self.resultTabel.horizontalHeader().setHighlightSections(False)
        self.resultTabel.horizontalHeader().setStretchLastSection(True)
        self.resultTabel.horizontalHeader().setDefaultSectionSize(65)
        self.resultTabel.horizontalHeader().setMinimumSectionSize(40)
        self.resultTabel.verticalHeader().setVisible(False)
        self.resultTabel.cellClicked.connect(self.showPreview)
        self.verticalLayout_2.addWidget(self.resultTabel)


        self.samplingButton = QtWidgets.QToolButton(self.middleFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.samplingButton.sizePolicy().hasHeightForWidth())
        self.samplingButton.setSizePolicy(sizePolicy)
        self.samplingButton.setMinimumSize(QtCore.QSize(0, 50))
        self.samplingButton.setFont(font)
        self.samplingButton.setObjectName("samplingButton")
        self.samplingButton.setText("فیش برداری از نتایج")
        self.samplingButton.setStyleSheet("""*{ border:none;
                                                border-radius:12px;
                                                background-color: %s;
                                                color: #FFFFFF;
                                              } 
                                             *:hover{background-color: %s;}
                                            """ %(BUTTON_BACKGROUND_COLOR, BUTTON_HOVER_BACKGROUND_COLOR))
        self.verticalLayout_4.addWidget(self.samplingButton)
        self.horizontalLayout.addWidget(self.middleFrame)

        self.rightFrame = QtWidgets.QFrame(self.centralwidget)
        self.rightFrame.setFrameShape(QtWidgets.QFrame.NoFrame)

        self.rightFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.rightFrame.setObjectName("rightFrame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.rightFrame)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(10)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout.addWidget(self.rightFrame)
        self.setCentralWidget(self.centralwidget)

        self.searchOption = mainSearchWidget(0 , self.whoosh, self.rightFrame)
        self.searchOption.openSignal.connect(self.onlyOneOptionOpen)
        self.searchOption.searchComplited.connect(self.fillTable)
        self.verticalLayout.addWidget(self.searchOption) 

        self.searchOption2 = selectDocuments(1, 'دامنه جست و جو', self.whoosh, parent=self.rightFrame)
        self.searchOption2.openSignal.connect(self.onlyOneOptionOpen)
        self.verticalLayout.addWidget(self.searchOption2) 

        self.searchOption3 = simpleOptionWidget(2, 'فیلتر نتایج', parent=self.rightFrame)
        self.searchOption3.openSignal.connect(self.onlyOneOptionOpen)
        self.verticalLayout.addWidget(self.searchOption3) 

        self.searchOption4 = simpleOptionWidget(3, 'بسته های الحاقی', parent=self.rightFrame)
        self.searchOption4.openSignal.connect(self.onlyOneOptionOpen)
        self.verticalLayout.addWidget(self.searchOption4) 

        self.junk = QtWidgets.QFrame(self.rightFrame)
        self.verticalLayout.addWidget(self.junk)

        self.horizontalLayout.setStretch(0, 20)
        self.horizontalLayout.setStretch(1, 10)
        self.horizontalLayout.setStretch(2, 8)

        self.searchDialog = None
        self.loadExistingdIndexers()
        



    def onlyOneOptionOpen(self, openID):
        all_buttons = [self.searchOption, self.searchOption2, self.searchOption3, self.searchOption4]
        all_buttons.remove(all_buttons[openID])
        for i in all_buttons:
            if not i.mainContainer.isHidden():
                i.toggleContainer()


    def F_AlignRight(self):
        self.textEdit.document().setDefaultTextOption(QtGui.QTextOption(QtCore.Qt.AlignRight))
        self.textEditAlign = 'right'
            
        self.alignRightButton.setStyleSheet(f"background-color: {BUTTON_BACKGROUND_COLOR};")
        self.alignLeftButton.setStyleSheet("")
        self.alignCenterButton.setStyleSheet("")
        print('align set to right')


    def F_AlignLeft(self):
        self.textEdit.document().setDefaultTextOption(QtGui.QTextOption(QtCore.Qt.AlignLeft))
        self.textEditAlign = 'left'

        self.alignRightButton.setStyleSheet("")
        self.alignLeftButton.setStyleSheet(f"background-color: {BUTTON_BACKGROUND_COLOR};")
        self.alignCenterButton.setStyleSheet("")
        print('align set to left')


    def F_AlignCenter(self):
        self.textEdit.document().setDefaultTextOption(QtGui.QTextOption(QtCore.Qt.AlignCenter))
        self.textEditAlign = 'center'

        self.alignRightButton.setStyleSheet("")
        self.alignLeftButton.setStyleSheet("")
        self.alignCenterButton.setStyleSheet(f"background-color: {BUTTON_BACKGROUND_COLOR};")
        print('align set to center')


    def fillTable(self):
        self.textEdit.clear()          # clear the preview every time search a new thing
        self.localSearchButton.setEnabled(False)     # disable the local search button untile user click on one of the results
        self.counterLabel.setText("0/0")
        self.fileNameLabel.setText('نام فایل')
        self.casesNumberLabel.setText(f'{len(self.whoosh.searchResult)} مورد')
        self.resultTabel.setRowCount(0)
        for result in self.whoosh.searchResult:
            self.resultTabel.insertRow(self.resultTabel.rowCount())
            self.addRow(result)
        
        self.resultTabel.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.resultTabel.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        self.resultTabel.horizontalHeader().setSectionResizeMode(2, QtWidgets.QHeaderView.ResizeToContents)
        self.resultTabel.horizontalHeader().setSectionResizeMode(3, QtWidgets.QHeaderView.ResizeToContents)


    def addRow(self, data):
        row = self.resultTabel.rowCount()-1
        item = QtWidgets.QTableWidgetItem(str(data['path'].split('/')[-1].split('.')[0]))  #, data['path'], data['score'], data['size'], data['content'], data['hightlightContent']
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        item.setData(QtCore.Qt.UserRole, data['path'])
        self.resultTabel.setItem(row, 3, item)

        item = QtWidgets.QTableWidgetItem(str(data['score']))
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        item.setData(QtCore.Qt.UserRole, data['path'])
        self.resultTabel.setItem(row, 2, item)

        # item = QtWidgets.QTableWidgetItem(self.createProperSize(data['size']))
        item = customTableWidgetItem(self.createProperSize(data['size']))
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        item.setData(QtCore.Qt.UserRole, data['size'])
        self.resultTabel.setItem(row, 1, item)

        item = QtWidgets.QTableWidgetItem(str(data['path'].split('/')[-1]))
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        item.setData(QtCore.Qt.UserRole, data['path'])
        self.resultTabel.setItem(row, 0, item)


    def createProperSize(self, sizeInByte):
        unit_symbols = ['byte', 'KB', 'MB', 'TB']
        for power in range(4):
            a = int(sizeInByte / (pow(1024, power)))
            if a < 1024:
                break

        power = power if power<4 else 3
        symbol = unit_symbols[power]

        num = round(sizeInByte/(pow(1024, power)), 2)
        return f'{num}{symbol}'


    def showPreview(self, row, col):
        if self.isLocalSearchEctivated:
            self.searchDialog.inputLine.setText("")
            self.searchDialog.close()
        print('-'*100)
        # self.textEdit.setAcceptRichText(True)
        self.localSearchButton.setEnabled(True)
        self.nextButton.setEnabled(True)
        self.previousButton.setEnabled(True)
        self.previewFilePath = self.resultTabel.item(row, 0).data(QtCore.Qt.UserRole)
        self.fileNameLabel.setText(self.previewFilePath.split('/')[-1])
        for r in self.whoosh.searchResult:
            if r['path'] == self.previewFilePath:
                self.content = r['content']
                self.previewText = r['hightlightContent']
                #self.previewPositions = r['positions'] #not required
                self.previewCurrentItemNumber = -1
                self.previewCurrentItemPosition = 0
                break

        if not self.previewText:
            print('highlight is emplty')
            self.previewText = f'<span style=" background-color:{GLOBAL_SEARCH_HIGHLIGHT_COLOR};">' + self.content + '</span>'
            self.previewPositions = [(0,len(self.previewText)), (0,len(self.previewText))]
        
        # print(len(self.previewPositions))
        #self.allItemsNumber = int(len(self.previewPositions)/2) #set this value using another method in F_GlobalSearchNextItem
        #self.counterLabel.setText(f"0/{self.allItemsNumber}") 
        self.previewText = self.previewText.replace('\n', '<br>')
        self.textEdit.setHtml(self.previewText)

        self.F_GlobalSearchNextItem()

        if self.textEditAlign == 'right':
            self.textEditAlign = None
            self.F_AlignRight()

        if self.textEditAlign == 'left':
            self.textEditAlign = None
            self.F_AlignLeft()

        if self.textEditAlign == 'center':
            self.textEditAlign = None
            self.F_AlignCenter()


    def clearLastSelectedItem(self, HTML):
        highlightTag = f'<span style=" background-color:{SELECTED_SEARCH_HIGHLIGHT_COLOR};">'
        pos = HTML.find(highlightTag)

        if self.isLocalSearchEctivated:
            c = 0
            while True:
                if HTML[pos + len(highlightTag) + c] == '<':
                    break
                else:
                    c += 1
            print(f'#############\"{HTML[pos + len(highlightTag) : pos + len(highlightTag) + c]}\"      \"{self.searchDialog.word}\"')
            if HTML[pos + len(highlightTag) : pos + len(highlightTag) + c] == self.searchDialog.word:
                HTML = HTML[:pos] + f'<span style=" background-color:{LOCAL_SEARCH_HIGHLIGHT_COLOR};">' + HTML[pos+len(highlightTag):]
            else:
                HTML = HTML[:pos] + f'<span style=" background-color:{GLOBAL_SEARCH_HIGHLIGHT_COLOR};">' + HTML[pos+len(highlightTag):]

        else:
            if pos != -1:
                HTML = HTML[:pos] + f'<span style=" background-color:{GLOBAL_SEARCH_HIGHLIGHT_COLOR};">' + HTML[pos+len(highlightTag):]
            else:
                print('blue not found')

        return HTML


    def F_GlobalSearchNextItem(self):
        HTML = self.textEdit.toHtml()
        print(f'number was: {self.previewCurrentItemNumber}')

        HTML = self.clearLastSelectedItem(HTML)

        highlightTag = f'<span style=" background-color:{GLOBAL_SEARCH_HIGHLIGHT_COLOR};">'    
        
        self.allItemsNumber = HTML.count(highlightTag) #set this value using the number of highlighted text elements
        self.counterLabel.setText(f"0/{self.allItemsNumber}")
        start = self.previewCurrentItemPosition
        for i in range(2):
            if self.previewCurrentItemNumber == -1:
                pos = -1
            else:
                pos = HTML.find(highlightTag, start)

            if pos == -1:
                print(pos)
                self.previewCurrentItemNumber = 0
                start = 0
                pos = HTML.find(highlightTag, start)
                print('end of file')
                break
            
            else:
                start = pos + len(highlightTag)
        else:
            self.previewCurrentItemNumber += 1


        print(f'number is: {self.previewCurrentItemNumber}      position is: {self.previewCurrentItemPosition}')
        self.previewCurrentItemPosition = pos
        self.counterLabel.setText(f'{self.previewCurrentItemNumber+1}/{self.allItemsNumber}')
        newHTML = HTML[:pos] + f'<span style=" background-color:{SELECTED_SEARCH_HIGHLIGHT_COLOR};">' + HTML[pos+len(highlightTag):]
        self.textEdit.clear()
        self.textEdit.setHtml(newHTML)


        hidenTextEdit = QtWidgets.QTextEdit()
        newHTML = HTML[:pos]
        hidenTextEdit.setHtml(newHTML)
        words = len(hidenTextEdit.toPlainText())

        cursor = self.textEdit.textCursor()
        cursor.movePosition(QtGui.QTextCursor.Start, QtGui.QTextCursor.MoveAnchor)
        cursor.movePosition(QtGui.QTextCursor.NextCharacter, QtGui.QTextCursor.MoveAnchor, words)
        self.textEdit.setTextCursor(cursor)



        
    def F_GlobalSearchPreviousItem(self):
        HTML = self.textEdit.toHtml()
        print(f'number was: {self.previewCurrentItemNumber}')

        HTML = self.clearLastSelectedItem(HTML)

        highlightTag = f'<span style=" background-color:{GLOBAL_SEARCH_HIGHLIGHT_COLOR};">'
        start = 0
        if self.previewCurrentItemNumber == 0:
            self.previewCurrentItemNumber = self.allItemsNumber-1
        else:
            self.previewCurrentItemNumber -= 1


        for i in range(self.previewCurrentItemNumber+1):
            pos = HTML.find(highlightTag, start)
            if pos != -1:
                start = pos + len(highlightTag)
            else:
                print('some error occured:(')
                return

        print(f'number is: {self.previewCurrentItemNumber}      position is: {self.previewCurrentItemPosition}')
        self.previewCurrentItemPosition = pos
        self.counterLabel.setText(f'{self.previewCurrentItemNumber+1}/{self.allItemsNumber}')
        
        newHTML = HTML[:pos] + f'<span style=" background-color:{SELECTED_SEARCH_HIGHLIGHT_COLOR};">' + HTML[pos+len(highlightTag):]
        self.textEdit.clear()
        self.textEdit.setHtml(newHTML)


        hidenTextEdit = QtWidgets.QTextEdit()
        newHTML = HTML[:pos]
        hidenTextEdit.setHtml(newHTML)
        words = len(hidenTextEdit.toPlainText())

        cursor = self.textEdit.textCursor()
        cursor.movePosition(QtGui.QTextCursor.Start, QtGui.QTextCursor.MoveAnchor)
        cursor.movePosition(QtGui.QTextCursor.NextCharacter, QtGui.QTextCursor.MoveAnchor, words)
        self.textEdit.setTextCursor(cursor)



    def showLocalSearchWindow(self):
        if not self.searchDialog:
            self.localSearchButton.setStyleSheet("background-color: %s" %(BUTTON_BACKGROUND_COLOR))
            self.searchDialog = localSearchDialog(self)
            self.searchDialog.move(27, 85)
            self.searchDialog.inputLine.returnPressed.connect(self.F_Find)
            self.searchDialog.nextButton.clicked.connect(self.F_LocalSearchNextItem)
            self.searchDialog.previousButton.clicked.connect(self.F_LocalSearchPreviousItem)
            self.searchDialog.clearScene.connect(self.clearLoclSearch)
            self.searchDialog.exec_()
            self.searchDialog = None
            self.localSearchButton.setStyleSheet("")
        else:
            self.searchDialog.inputLine.setText("")
            self.searchDialog.close()


    def F_Find(self):
        self.isLocalSearchEctivated = True
        text = self.searchDialog.inputLine.text()
        self.searchDialog.word = text
        if self.previewFilePath and text:
            start = 0
            self.originalHTML = self.textEdit.toHtml()
            new_html = ''
            while True:
                item = self.originalHTML.find(text, start)
                if item != -1:
                    self.searchDialog.allLocalSearchItems.append(item)
                    new_html += self.originalHTML[start:item] + f'<span style=" background-color:{LOCAL_SEARCH_HIGHLIGHT_COLOR};">{text}</span>'
                    start = item + len(text)
                else:
                    new_html += self.originalHTML[start:]
                    break
            
            self.textEdit.setHtml(new_html)
            self.searchDialog.number.setText(f"0/{len(self.searchDialog.allLocalSearchItems)}")

            if len(self.searchDialog.allLocalSearchItems) > 0:
                self.searchDialog.nextButton.setEnabled(True)
                self.searchDialog.previousButton.setEnabled(True)
                self.F_LocalSearchNextItem()


    def F_LocalSearchNextItem(self):
        HTML = self.textEdit.toHtml()
        if len(self.searchDialog.allLocalSearchItems) > 0:
            HTML = self.clearLastSelectedItem(HTML)

            if self.searchDialog.currentHighlightItemNumber == len(self.searchDialog.allLocalSearchItems)-1:
                self.searchDialog.currentHighlightItemNumber = 0
            else:
                self.searchDialog.currentHighlightItemNumber += 1

            highlightTag = f'<span style=" background-color:{LOCAL_SEARCH_HIGHLIGHT_COLOR};">'
            start = 0
            for i in range(self.searchDialog.currentHighlightItemNumber+1):
                pos = HTML.find(highlightTag, start)

                if pos == -1:
                    print(pos)
                    start = 0
                    pos = HTML.find(highlightTag, start)
                    print('end of file')
                    break
                
                else:
                    start = pos + len(highlightTag)


            self.searchDialog.number.setText(f'{self.searchDialog.currentHighlightItemNumber+1}/{len(self.searchDialog.allLocalSearchItems)}')
            newHTML = HTML[:pos] + f'<span style=" background-color:{SELECTED_SEARCH_HIGHLIGHT_COLOR};">' + HTML[pos+len(highlightTag):]
            self.textEdit.clear()
            self.textEdit.setHtml(newHTML)


            hidenTextEdit = QtWidgets.QTextEdit()
            newHTML = HTML[:pos]
            hidenTextEdit.setHtml(newHTML)
            words = len(hidenTextEdit.toPlainText())

            cursor = self.textEdit.textCursor()
            cursor.movePosition(QtGui.QTextCursor.Start, QtGui.QTextCursor.MoveAnchor)
            cursor.movePosition(QtGui.QTextCursor.NextCharacter, QtGui.QTextCursor.MoveAnchor, words)
            self.textEdit.setTextCursor(cursor)


    def F_LocalSearchPreviousItem(self):
        HTML = self.textEdit.toHtml()
        print(f'number was: {self.previewCurrentItemNumber}')

        HTML = self.clearLastSelectedItem(HTML)


        if self.searchDialog.currentHighlightItemNumber == 0:
            self.searchDialog.currentHighlightItemNumber = len(self.searchDialog.allLocalSearchItems)-1
        else:
            self.searchDialog.currentHighlightItemNumber -=1

        
        highlightTag = f'<span style=" background-color:{LOCAL_SEARCH_HIGHLIGHT_COLOR};">'
        start = 0
        for i in range(self.searchDialog.currentHighlightItemNumber+1):
            pos = HTML.find(highlightTag, start)
            if pos != -1:
                start = pos + len(highlightTag)
            else:
                print('some error occured:(')
                return

        self.searchDialog.number.setText(f'{self.searchDialog.currentHighlightItemNumber+1}/{len(self.searchDialog.allLocalSearchItems)}')
        
        newHTML = HTML[:pos] + f'<span style=" background-color:{SELECTED_SEARCH_HIGHLIGHT_COLOR};">' + HTML[pos+len(highlightTag):]
        self.textEdit.clear()
        self.textEdit.setHtml(newHTML)
        

        hidenTextEdit = QtWidgets.QTextEdit()
        newHTML = HTML[:pos]
        hidenTextEdit.setHtml(newHTML)
        words = len(hidenTextEdit.toPlainText())

        cursor = self.textEdit.textCursor()
        cursor.movePosition(QtGui.QTextCursor.Start, QtGui.QTextCursor.MoveAnchor)
        cursor.movePosition(QtGui.QTextCursor.NextCharacter, QtGui.QTextCursor.MoveAnchor, words)
        self.textEdit.setTextCursor(cursor)


    def clearLoclSearch(self):
        self.isLocalSearchEctivated = False
        self.previewCurrentItemNumber = -1
        self.previewCurrentItemPosition = 0
        self.counterLabel.setText(f"0/{self.allItemsNumber}")
        self.textEdit.setHtml(self.previewText)
        self.F_GlobalSearchNextItem()


    def loadExistingdIndexers(self):
        for folder in os.listdir("index"):
            path = "index/" + folder
            if os.path.isdir(path):
                try:
                    with open(path + '/fileInfo.json', 'r') as f:
                        fileDict = json.load(f)
                        self.whoosh.loadIndexer(path, fileDict['path'])
                        self.searchOption2.mainTree.addTree(fileDict)
                        print(f'{path} is loaded')
                except:
                    print(f'error occured with openning indexer [{path}]')







if __name__=="__main__":
    if not os.path.exists(os.path.join(tempfile.gettempdir(), 'tika-server.jar')):
        print('copy tika server to TMP file')
        shutil.copyfile(os.path.join(os.getcwd(), 'jars', 'tika-server.jar'), os.path.join(tempfile.gettempdir(), 'tika-server.jar'))

    if not os.path.exists(os.path.join(tempfile.gettempdir(), 'tika-server.jar.md5')):
        print('copy tika server md5 to TMP file')
        shutil.copyfile(os.path.join(os.getcwd(), 'jars', 'tika-server.jar.md5'), os.path.join(tempfile.gettempdir(), 'tika-server.jar.md5'))
        
    # if tika server wasn't run in background the program after runing the code bellow will sleep for 5 sec ! cant do any thing:(
    parser.from_file('tika_server_test.txt')
    app = QtWidgets.QApplication([])
    widget = Ui_MainWindow()
    widget.show()
    sys.exit(app.exec_())

