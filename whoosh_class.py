from constants import GLOBAL_SEARCH_HIGHLIGHT_COLOR, LOCAL_SEARCH_HIGHLIGHT_COLOR
from random import randint
import os
import json
import shutil
from whoosh import writing
from whoosh.index import create_in, open_dir
from whoosh.fields import *
from whoosh.qparser import QueryParser
from whoosh.highlight import WholeFragmenter, Formatter, get_text, Highlighter
from whoosh.query import Term
from whoosh.analysis.analyzers import StandardAnalyzer

from tika import parser
allItemspositions = []


class whooshIndexer:
    def __init__(self) -> None:
        self.allObjects = []      #{'rootPath':None, 'indexer':Ix, 'savePath':'C://'}
        self.searchResult = []    #[{'path':'C://', 'score':135, 'size':123, 'content':'', 'hightlightContent':''}]
        self.maskedPath = []
        self.savePath = None
        self.schema = None
        self.query = None
        self.makeSchema()


    def makeSchema(self):  # only make a schema once. it will use for all indexer
        ana=analysis.StandardAnalyzer(stoplist=None, minsize=0, maxsize=None) #make the searcher consider stop words such as 'a', 'you', ... in query in English
        self.schema = Schema(name=TEXT(stored=True), path=ID(stored=True, unique=True), size=NUMERIC(numtype=int, bits=64, stored=True), content=TEXT(stored=True, analyzer=ana))

    def makeIndexDir(self, filePath):
        self.filePath = filePath
        folderToSave = os.path.basename(self.filePath) + str(randint(0, 100000))
        self.savePath = os.path.join(os.getcwd(), 'index', folderToSave)

        try:
            os.makedirs(self.savePath)
        except:
            print(F'an error occured for create folder : {self.savePath}')
        

        with open(os.path.join(self.savePath, 'index-name.txt'), 'w') as f:
            f.write(os.path.basename(self.filePath))

        # with open(os.path.join(self.savePath, 'fileInfo.json'), 'w') as f:
        #     pass

    def createIndexer(self):
        self.indexer = create_in(self.savePath, self.schema)
        self.allObjects.append({'rootPath':self.filePath, 'indexer':self.indexer, 'savePath':self.savePath})
        print(self.allObjects[-1])
    
    def loadIndexer(self, dir, rootPath):
        self.indexer = open_dir(dir)
        self.allObjects.append({'rootPath':rootPath, 'indexer':self.indexer, 'savePath':dir})
        print(f'file {dir} is loaded')
    
    def saveFileInfo(self, infoDict:dict):
        with open(os.path.join(self.savePath, 'fileInfo.json'), 'w') as f:
            json.dump(infoDict, f)

    def indexDocument(self, ix, filePath):
        writer = ix.writer()
        content = readFileContent(filePath)
        name = os.path.basename(filePath)
        size = os.path.getsize(filePath)
        writer.add_document(name=name, path=filePath, size=size, content=content)
        # print(f'{name} indexed :)')
        writer.commit(optimize=True)

    def indexDocuments(self, ix, filesPath):
        for path in filesPath:
            self.indexDocument(ix, path)

    
    def deleteIndexer(self, rootPath):      #should free the ram the indexer is occupied
        for indexerInfo in self.allObjects:
            if indexerInfo['rootPath'] == rootPath:
                indexerInfo['indexer'].writer().commit(mergetype=writing.CLEAR)
                shutil.rmtree(indexerInfo['savePath'])
                self.allObjects.remove(indexerInfo)
                print(f'file {rootPath} is deleted from indexers')
                break

    
    def updateOneDocument(self, ix, filePath):    #i dont use this function :(
        writer = ix.writer()
        name = os.path.basename(filePath)
        content = readFileContent(filePath)
        size = os.path.getsize(filePath)
        writer.update_document(name=name, path=filePath, size=size, content=content)
        print(f'{filePath} updated')
        writer.commit()
        

    def makeQuery(self, text):
        print('--- query: ', text)
        customParser = QueryParser('content', self.schema)
        self.query = customParser.parse(text)

    def search(self):
        global allItemspositions
        self.searchResult = []
        if self.allObjects:
            for i in self.allObjects:
                indexer = i['indexer']
                if self.maskedPath:
                    mask = Term('path', self.maskedPath[0])
                    for path in self.maskedPath[1:]:
                        mask |= Term('path', path)
                else:
                    mask = None


                with indexer.searcher() as searcher:
                    results = searcher.search(self.query, mask=mask, limit=None, terms=True)

    
                    for hit in results:

                        hi = Highlighter(fragmenter=WholeFragmenter(charlimit=None), formatter=globalFormatter()) #highlighter object
                        highlightContent = hi.highlight_hit(hit, 'content', minscore=-1,  strict_phrase=True) #set strict_phrase to True to avoid highlighting the terms matched with the phrase's terms
                        # minscore = -1 -> retrun the original content if the phrase has not found in the content, to avoid null 'highlightContent'

                        self.searchResult.append({'path':hit['path'],
                                                  'score':int(hit.score*100),
                                                  'size':hit['size'],
                                                  'content':hit['content'],
                                                  'hightlightContent': highlightContent,
                                                  #'positions':allItemspositions #not required
                                                  }) 
                        
                        
                        print(f"{hit['path']}  {len(hit['content'])}  {len(hit.highlights('content'))}" )
                        #allItemspositions = [] #not required
                        
        else:
            print('noting found')

        return True
        



class globalFormatter(Formatter):
    def format_token(self, text, token, replace=False):
        # Use the get_text function to get the text corresponding to the
        
        #allItemspositions.append((token.startchar, token.endchar)) not required
        # print('F')
        tokentext = get_text(text, token, replace)
        # Return the text as you want it to appear in the highlighted
        return f'<span style=" background-color:{GLOBAL_SEARCH_HIGHLIGHT_COLOR};">{tokentext} </span>' #add an space to connect the highlighted terms







def readFileContent(filePath) -> str:
    text = parser.from_file(filePath)
    if text['content']:
        output = text['content'].strip()

        print(f'{filePath} content added [{len(output)}]')
        return output
    output = str(text['metadata']).strip()
    print(f'{filePath} metadata added [{len(output)}]')

    return output
    




