from PyQt5 import QtCore, QtGui, QtWidgets
from constants import BORDER_COLOR, OPTION_BACKGROUND_COLOR, OPTION_HEIGHT, GENERAL_FONT_NAME, OPTION_FONT, BUTTON_BACKGROUND_COLOR, BUTTON_HOVER_BACKGROUND_COLOR
import os
import subprocess


def getMainApp(a):
    global app
    app = a

class customTableWidgetItem(QtWidgets.QTableWidgetItem):
    def __init__(self, t) -> None:
        super().__init__(t)
        

    def __lt__(self, other):
        return (self.data(QtCore.Qt.UserRole) < other.data(QtCore.Qt.UserRole))


class customTreeWidgetItem(QtWidgets.QTreeWidgetItem):
    def __init__(self, t, path, isFile=True, parent= None):
        super().__init__(t)
        self.path = path
        self.isFile = isFile
        self.bigDad = None     # the main root



class customTreeWidget(QtWidgets.QTreeWidget):
    def __init__(self, whooshObj, parent= None) -> None:
        super().__init__(parent=parent)
        self.whoosh = whooshObj
        self.all_trees = []

        self.folderPixmap = QtGui.QPixmap('image/folder.png')
        self.folderPixmap.scaled(24,24)
        self.filePixmap = QtGui.QPixmap('image/file.png')
        self.filePixmap.scaled(24, 24)
        self.folderIcon = QtGui.QIcon(self.folderPixmap)
        self.fileIcon = QtGui.QIcon(self.filePixmap)

        font = QtGui.QFont(GENERAL_FONT_NAME, 10)
        self.setFont(font)
        self.setFrameShape(QtWidgets.QFrame.NoFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.setColumnCount(1)
        self.header().setVisible(False)
        self.createRightClickMenu()
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.treeMousePressEvent)

        self.createRightClickMenu()
        self.createAdvanceRightClickMenu()
        self.itemChanged.connect(self.itemChangeState)
     
    def itemChangeState(self, treeItem, val):
        if treeItem.isFile:
            if treeItem.checkState(0) == 0:
                print(f'{treeItem.path} is unchecked ' )         # file is unchcked 
                self.whoosh.maskedPath.append(treeItem.path)

            elif treeItem.checkState(0) == 2:
                print(f'{treeItem.path} is checked' )            # file is checked 
                self.whoosh.maskedPath.remove(treeItem.path)

    def treeMousePressEvent(self, val):
        if self.itemAt(val):
            self.rihgtClickTreeWidgetItem = self.itemAt(val)
            self.advancerightClickMenu.exec_(QtGui.QCursor.pos())
        else:
            self.rightClickMenu.exec_(QtGui.QCursor.pos())

    def createRightClickMenu(self):
        self.rightClickMenu = QtWidgets.QMenu(self)
        self.subMenu = QtWidgets.QMenu(' ایجاد فهرست از', self.rightClickMenu)
        self.rightClickMenu.addMenu(self.subMenu)
        createAction = QtWidgets.QAction('پوشه', self)
        createAction.triggered.connect(self.selectDirectory)
        self.subMenu.addAction(createAction)

        if self.topLevelItemCount():
            refreshAction = QtWidgets.QAction('بازسازی فهرست', self)
            refreshAction.triggered.connect(self.refreshAllDirectoties)
            self.rightClickMenu.addAction(refreshAction)
    
    def createAdvanceRightClickMenu(self):
        self.advancerightClickMenu = QtWidgets.QMenu(self)
        self.advancesubMenu = QtWidgets.QMenu(' ایجاد فهرست از', self)
        self.advancerightClickMenu.addMenu(self.advancesubMenu)
        createAction = QtWidgets.QAction('پوشه', self.advancesubMenu)
        createAction.triggered.connect(self.selectDirectory)
        self.advancesubMenu.addAction(createAction)

        refreshAction = QtWidgets.QAction('بازسازی فهرست ها', self)
        refreshAction.triggered.connect(self.refreshAllDirectoties)
        self.advancerightClickMenu.addAction(refreshAction)

        self.advancerightClickMenu.addSeparator()

        deleteAction = QtWidgets.QAction('حذف فهرست', self)
        deleteAction.triggered.connect(self.deleteTree)
        self.advancerightClickMenu.addAction(deleteAction)
        
        openfolderAction = QtWidgets.QAction('باز کردن پوشه', self)
        openfolderAction.triggered.connect(self.openFolderInExplorer)
        self.advancerightClickMenu.addAction(openfolderAction)

    def addTree(self, files_dict):
        item = customTreeWidgetItem([files_dict['name']], files_dict['path'], isFile=False)
        self.mainRoot = files_dict['path']
        item.bigDad = self.mainRoot
        item.setFlags(item.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsSelectable)
        item.setCheckState(0, QtCore.Qt.Checked)
        item.setIcon(0, self.folderIcon)

        self.unpackFiles(item, files_dict['data'][files_dict['name']], files_dict['path'])
        self.insertTopLevelItem(self.topLevelItemCount(), item)
      
        # self.filesData = files_dict    #can be a option for updeting the trees
        self.createRightClickMenu()
        self.all_trees.append(item)

    def unpackFiles(self, item:customTreeWidgetItem, filesDict:dict, Dad):
        for key in filesDict.keys():
            if key != ' ':
                child = customTreeWidgetItem([key], Dad, isFile=False)
                child.bigDad = self.mainRoot
                item.addChild(child)
                child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsSelectable)
                child.setCheckState(0, QtCore.Qt.Checked)
                child.setIcon(0, self.folderIcon)
                self.unpackFiles(child, filesDict[key], Dad + f'/{key}')

        for file in filesDict[' ']:
            child = customTreeWidgetItem([file.split('/')[-1]], file, isFile=True)
            child.bigDad = self.mainRoot
            child.setFlags(child.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)
            child.setCheckState(0, QtCore.Qt.Checked)
            child.setIcon(0, self.fileIcon)
            item.addChild(child)

    def openFolderInExplorer(self):
        cmd = r'explorer /select,"%s"' % (self.rihgtClickTreeWidgetItem.path.replace('/', '\\'))
        subprocess.Popen(cmd)

    def deleteTree(self):
        print(self.rihgtClickTreeWidgetItem.bigDad)
        dialog = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, 'حذف فهرست', f'آیا می خواهید سند  {self.rihgtClickTreeWidgetItem.bigDad.split("/")[-1]}  از دامنه جست و جو حذف شود؟', QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        ans = dialog.exec_()
        if ans == QtWidgets.QMessageBox.Yes:
            for i in range(self.topLevelItemCount()):
                if self.topLevelItem(i).path == self.rihgtClickTreeWidgetItem.bigDad:
                    self.all_trees.remove(self.topLevelItem(i))
                    self.whoosh.deleteIndexer(self.topLevelItem(i).path)
                    self.takeTopLevelItem(i)
                    break
            self.createRightClickMenu()

    def refreshAllDirectoties(self):
        refreshList = []
        for index in reversed(range(self.topLevelItemCount())):
            refreshList.append(self.topLevelItem(index).path)
            self.whoosh.deleteIndexer(self.topLevelItem(index).path)
            self.takeTopLevelItem(index)
        
        for path in reversed(refreshList):
            fileDict = {'name': path.split('/')[-1], 'path': path, 'data':{}}
            fileDict['data'][fileDict['name']] = {' ': []}
            self.whoosh.makeIndexDir(path)
            self.whoosh.createIndexer()
            self.findFiles(path, fileDict['data'][fileDict['name']])
            self.whoosh.saveFileInfo(fileDict)
            self.addTree(fileDict)
        
        self.createRightClickMenu()

    def findFiles(self, folderPath, savedict):
        for file in os.listdir(folderPath):
            if os.path.isfile(folderPath + '/' + file):
                savedict[' '].append(folderPath + '/' + file)
                self.whoosh.indexDocument(self.whoosh.indexer, folderPath + '/' + file)
                app.processEvents()

            else:
                savedict[file] = {' ':[]}
                self.findFiles(folderPath + '/' + file,  savedict[file])

    def selectDirectory(self):
        dir = QtWidgets.QFileDialog.getExistingDirectory(self, 'select folder')
        if dir:
            dialog = scanDirectoryDialog(dir, self.whoosh, parent = self)
            dialog.setWindowModality(QtCore.Qt.ApplicationModal)
            dialog.finishSiganl.connect(self.addTree)
            dialog.exec_()



class customButton(QtWidgets.QFrame):
    openSignal = QtCore.pyqtSignal(int)
    def __init__(self, ID:int, parent=None) -> None:
        super().__init__(parent=parent)
        self.ID = ID
        self.setObjectName("mainFrame")
        self.setStyleSheet("#mainFrame{border: 3px solid %s; border-radius: 14px; background-color: %s} " %(BORDER_COLOR, OPTION_BACKGROUND_COLOR))
        
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)

        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")

        self.topFrame = QtWidgets.QFrame(self)
        self.topFrame.setFixedHeight(OPTION_HEIGHT)
        self.topFrame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.topFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.topFrame.setObjectName("topFrame")

        self.horizontalLayout = QtWidgets.QHBoxLayout(self.topFrame)
        self.horizontalLayout.setContentsMargins(10, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.extendButton = QtWidgets.QPushButton(self.topFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.extendButton.sizePolicy().hasHeightForWidth())
        self.extendButton.setSizePolicy(sizePolicy)
        self.extendButton.setStyleSheet("border:none;")
        self.plusIcon = QtGui.QIcon()
        self.plusIcon.addPixmap(QtGui.QPixmap("image/plus.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.minusIcon = QtGui.QIcon()
        self.minusIcon.addPixmap(QtGui.QPixmap("image/minus.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.extendButton.setIcon(self.plusIcon)
        self.extendButton.setIconSize(QtCore.QSize(24, 24))
        self.extendButton.setObjectName("extendButton")
        self.extendButton.clicked.connect(self.toggleContainer)
        self.horizontalLayout.addWidget(self.extendButton)

        self.topContainer = QtWidgets.QFrame(self.topFrame)
        self.topContainer.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.topContainer.setFrameShadow(QtWidgets.QFrame.Raised)
        self.topContainer.setObjectName("topContainer")

        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.topContainer)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")

        self.horizontalLayout.addWidget(self.topContainer)
        self.verticalLayout.addWidget(self.topFrame)

        self.mainContainer = QtWidgets.QFrame(self)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mainContainer.sizePolicy().hasHeightForWidth())
        self.mainContainer.setSizePolicy(sizePolicy)
        self.mainContainer.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.mainContainer.setFrameShadow(QtWidgets.QFrame.Raised)
        self.mainContainer.setObjectName("mainContainer")
        self.mainContainer.hide()
        self.verticalLayout.addWidget(self.mainContainer)

        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.mainContainer)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")

        self.topFrame.mousePressEvent = self.topFrameMousePressEvent

    def topFrameMousePressEvent(self, event) -> None:
        if event.buttons() == QtCore.Qt.LeftButton:
            self.toggleContainer()
        return super().mousePressEvent(event)

    def toggleContainer(self):
        
        if self.mainContainer.isHidden():
            self.mainContainer.show()
            self.extendButton.setIcon(self.minusIcon)

            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
            self.setSizePolicy(sizePolicy)
            self.openSignal.emit(self.ID)

        else:
            self.mainContainer.hide()
            self.extendButton.setIcon(self.plusIcon)

            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
            self.setSizePolicy(sizePolicy)



class mainSearchWidget(customButton):
    searchComplited = QtCore.pyqtSignal()
    def __init__(self, ID, whooshObj, parent=None) -> None:
        super().__init__(ID, parent=parent)
        self.whoosh = whooshObj
        self.horizontalLayout_2.setContentsMargins(10, 0, 10, 0)
        self.horizontalLayout_2.setSpacing(10)

        self.searchInput = QtWidgets.QLineEdit(self.topContainer)
        self.searchInput.setStyleSheet('border:none; border-bottom: 2px solid #dddddd;')
        font = QtGui.QFont(OPTION_FONT[0], OPTION_FONT[1])
        self.searchInput.setFont(font)
        self.searchInput.setPlaceholderText('واژه مورد نظر را وارد کنید')
        # self.searchInput.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.searchInput.textChanged.connect(self.activateBasicSearchButton)
        self.searchInput.returnPressed.connect(self.basicSearching)
        self.horizontalLayout_2.addWidget(self.searchInput)


        self.searchButton = QtWidgets.QPushButton(self.topContainer)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.searchButton.sizePolicy().hasHeightForWidth())
        self.searchButton.setSizePolicy(sizePolicy)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("image/whiteSearch.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.searchButton.setIcon(icon)
        self.searchButton.setIconSize(QtCore.QSize(24, 24))
        self.searchButton.setStyleSheet("""*{   background-color: %s;
                                                border:none;
                                                padding: 10px;
                                                border-radius:15px;
                                                }
                                            *:hover{background-color: %s;
                                                }""" %(BUTTON_BACKGROUND_COLOR, BUTTON_HOVER_BACKGROUND_COLOR))
        self.searchButton.setEnabled(False)
        self.searchButton.clicked.connect(self.basicSearching)
        self.horizontalLayout_2.addWidget(self.searchButton)

        self.label = QtWidgets.QLabel(self.topContainer)
        self.label.setText('جست و جوی پیشرفته')
        
        font = QtGui.QFont(OPTION_FONT[0], OPTION_FONT[1])
        self.label.setFont(font)
        self.horizontalLayout_2.addWidget(self.label)
        self.label.hide()

        #design inside of advanced search
        self.verticalLayout_2.setContentsMargins(7, 7, 7, 7)

        self.topMainContainerFrame = QtWidgets.QFrame(self.mainContainer)
        # self.topMainContainerFrame.setStyleSheet("background-color: #a2145d")
        self.verticalLayout_2.addWidget(self.topMainContainerFrame, 0, QtCore.Qt.AlignTop)

        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.topMainContainerFrame)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setSpacing(10)

        self.allWordInput = advanceInputWidget("همه عبارات", self.topMainContainerFrame)
        self.verticalLayout_3.addWidget(self.allWordInput)

        self.oneWordsInput = advanceInputWidget("یکی از عبارات", self.topMainContainerFrame)
        self.verticalLayout_3.addWidget(self.oneWordsInput)

        self.anyWordsInput = advanceInputWidget("هیچکدام از عبارات", self.topMainContainerFrame)
        self.verticalLayout_3.addWidget(self.anyWordsInput)

        self.exactExpInput = advanceInputWidget("عین عبارات", self.topMainContainerFrame)
        self.verticalLayout_3.addWidget(self.exactExpInput)


        self.bottomMainContainerFrame = QtWidgets.QFrame(self.mainContainer)
        self.bottomMainContainerFrame.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.verticalLayout_2.addWidget(self.bottomMainContainerFrame, 0, QtCore.Qt.AlignBottom)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.bottomMainContainerFrame)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setSpacing(10)

        self.spaceBetweenLetterLabel = QtWidgets.QLabel(self.bottomMainContainerFrame)
        self.spaceBetweenLetterLabel.setText("فاصله بین کلمات")
        font = QtGui.QFont(GENERAL_FONT_NAME, 10)
        self.spaceBetweenLetterLabel.setFont(font)
        self.spaceBetweenLetterLabel.setStyleSheet("color: %s;" %(BUTTON_BACKGROUND_COLOR))
        self.verticalLayout_4.addWidget(self.spaceBetweenLetterLabel)

        self.spaceBetweenLetterInputLine = QtWidgets.QLineEdit(self.bottomMainContainerFrame)
        self.spaceBetweenLetterInputLine.setStyleSheet("border: none;")
        self.spaceBetweenLetterInputLine.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.spaceBetweenLetterInputLine.setText('0')
        self.spaceBetweenLetterInputLine.setValidator(QtGui.QIntValidator(0, 9999))
        self.verticalLayout_4.addWidget(self.spaceBetweenLetterInputLine)

        self.searchButton2 = QtWidgets.QPushButton(self.bottomMainContainerFrame)
        self.searchButton2.setIcon(icon)
        self.searchButton2.setIconSize(QtCore.QSize(24, 24))
        self.searchButton2.setStyleSheet("""*{   background-color: %s;
                                                border:none;
                                                padding: 10px;
                                                border-radius:12px;
                                                }
                                            *:hover{background-color: %s;
                                                }""" %(BUTTON_BACKGROUND_COLOR, BUTTON_HOVER_BACKGROUND_COLOR))
        self.searchButton2.setEnabled(False)
        self.searchButton2.clicked.connect(self.advanceSearching)
        self.verticalLayout_4.addWidget(self.searchButton2)

        self.allWordInput.mainInput.textChanged.connect(self.activateAdvanceSearchButton)
        self.oneWordsInput.mainInput.textChanged.connect(self.activateAdvanceSearchButton)
        self.anyWordsInput.mainInput.textChanged.connect(self.activateAdvanceSearchButton)
        self.exactExpInput.mainInput.textChanged.connect(self.activateAdvanceSearchButton)

    def mousePressEvent(self, event) -> None:
        return QtWidgets.QFrame.mousePressEvent(self, event)

    def toggleContainer(self):
        if self.mainContainer.isHidden():
            self.horizontalLayout_2.setContentsMargins(10, 0, 20, 0)
            self.searchButton.hide()
            self.searchInput.hide()
            self.label.show()
        else:
            self.horizontalLayout_2.setContentsMargins(10, 0, 10, 0)
            self.label.hide()
            self.searchButton.show()
            self.searchInput.show()
        return super().toggleContainer()
        
    def basicSearching(self):
        if len(self.searchInput.text().strip()) >1:
            queryText = self.searchInput.text().strip()
            self.whoosh.makeQuery(queryText)
            result = self.whoosh.search()
            if result:
                self.searchComplited.emit()

    def advanceSearching(self):
        print('ddsdd')
        allWordText = self.extractWords(self.allWordInput.mainInput.text())
        if not self.allWordInput.accuracyCkeckBox.isChecked() and allWordText:
            allWordText = self.addStar(allWordText)
            
        oneWordText = self.extractWords(self.oneWordsInput.mainInput.text())
        if not self.oneWordsInput.accuracyCkeckBox.isChecked() and oneWordText:
            oneWordText = self.addStar(oneWordText)

        anyWordText = self.extractWords(self.anyWordsInput.mainInput.text())
        if not self.anyWordsInput.accuracyCkeckBox.isChecked() and anyWordText:
            anyWordText = self.addStar(anyWordText)

        exactExpText = self.extractWords(self.exactExpInput.mainInput.text())


        spaceBetween = int(self.spaceBetweenLetterInputLine.text())
        queryText = self.makeQueryText(allWordText, oneWordText, anyWordText, exactExpText, spaceBetween)
        print(queryText)
        self.whoosh.makeQuery(queryText)
        result = self.whoosh.search()
        if result:
            self.searchComplited.emit()
        
    def makeQueryText(self, allWordText, oneWordText, anyWordText, exactExpText, spaceBetween):
        if allWordText:
            allWordText = allWordText.replace(' ', ' AND ')
            allWordText = f'({allWordText}) AND '
        if oneWordText:
            oneWordText = oneWordText.replace(' ', ' OR ')
            oneWordText  = f'({oneWordText}) AND '
        if anyWordText:
            anyWordText = anyWordText.replace(' ', ' OR ')
            anyWordText = f'(NOT ({anyWordText})) AND '
        if exactExpText:
            exactExpText = f'("{exactExpText}") AND '

        text = allWordText + oneWordText + anyWordText + exactExpText 
        text = text[:-4]

        if spaceBetween !=0:
            text = f'\"{text}\"~{spaceBetween+1}'
        
        print(text)
        return text

    def extractWords(self, text:str):    # delete the space at start and end of input. also delete more than one space in between of the words
        a = ''
        for word in text.strip().split(' '):
            if word != '':
                a = a + word + ' '

        return a.strip()

    def addStar(self, text:str):    #add * in the end of each word in a sentence
        return '*' + text.replace(' ', '* *') + '*'

    def activateBasicSearchButton(self, text):
        if len(text) >1:
            self.searchButton.setEnabled(True)
        else:
            self.searchButton.setEnabled(False)

    def activateAdvanceSearchButton(self, text):
        if len(self.allWordInput.mainInput.text().strip()) >1 or len(self.oneWordsInput.mainInput.text().strip()) >1 or len(self.anyWordsInput.mainInput.text().strip()) >1:
            self.searchButton2.setEnabled(True)
        else:
            self.searchButton2.setEnabled(False)


class advanceInputWidget(QtWidgets.QFrame):
    def __init__(self, text,  parent=None) -> None:
        super().__init__(parent=parent)
        self.text = text
        self.setObjectName('advanceInputWidget')
        self.VLay = QtWidgets.QVBoxLayout(self)
        self.VLay.setContentsMargins(0, 0, 0, 0)
        self.VLay.setSpacing(0)

        self.label = QtWidgets.QLabel(self)
        self.label.setStyleSheet("color: %s;" %(BUTTON_BACKGROUND_COLOR))
        self.label.setText(' ')
        self.label.setFont(QtGui.QFont(GENERAL_FONT_NAME, 10))
        self.VLay.addWidget(self.label)

        self.frame = QtWidgets.QFrame(self)
        self.VLay.addWidget(self.frame)

        self.HLay = QtWidgets.QHBoxLayout(self.frame)
        self.HLay.setContentsMargins(0, 0, 0, 0)
        self.HLay.setSpacing(10)
        
        if self.text != "عین عبارات":
            self.accuracyCkeckBox = QtWidgets.QCheckBox(self.frame)
            self.accuracyCkeckBox.setLayoutDirection(QtCore.Qt.RightToLeft)
            self.accuracyCkeckBox.setFont(QtGui.QFont(GENERAL_FONT_NAME, 10))
            self.accuracyCkeckBox.setText("دقیق")
            self.accuracyCkeckBox.setChecked(True)
            self.HLay.addWidget(self.accuracyCkeckBox)

        self.mainInput = QtWidgets.QLineEdit(self.frame)
        self.mainInput.setFont(QtGui.QFont(GENERAL_FONT_NAME, 11))
        self.mainInput.setStyleSheet('border: none;')
        self.mainInput.setPlaceholderText(self.text)
        self.mainInput.focusInEvent = self.customfocusInEvent
        self.mainInput.focusOutEvent = self.customfocusOutEvent
        self.HLay.addWidget(self.mainInput)

        self.line = QtWidgets.QFrame(self)
        self.line.setStyleSheet("background-color: #f0f0f0;")
        self.line.setLineWidth(0)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.VLay.addWidget(self.line)

    def customfocusInEvent(self, event) -> None:
        self.label.setText(self.text)
        self.mainInput.setPlaceholderText(' ')
        return QtWidgets.QLineEdit.focusInEvent(self.mainInput, event)

    def customfocusOutEvent(self, event) -> None:
        self.label.setText(' ')
        self.mainInput.setPlaceholderText(self.text)
        return QtWidgets.QLineEdit.focusOutEvent(self.mainInput, event)



class selectDocuments(customButton):
    fileSignal = QtCore.pyqtSignal(dict)  # files dict, index data
    
    def __init__(self, ID, text, whooshObj,  parent=None) -> None:
        super().__init__(ID, parent=parent)
        self.all_trees = []
        # self.whoosh = whooshObj
        
        self.label = QtWidgets.QLabel(self.topContainer)
        self.label.setText(text)
        font = QtGui.QFont(OPTION_FONT[0], OPTION_FONT[1])
        self.label.setFont(font)

        self.horizontalLayout_2.addWidget(self.label)
        self.horizontalLayout_2.setContentsMargins(0, 0, 20, 0)

        self.mainContainer.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.mainTree = customTreeWidget(whooshObj, self.mainContainer)
        self.verticalLayout_2.addWidget(self.mainTree)



class scanDirectoryDialog(QtWidgets.QDialog):
    directorySignal = QtCore.pyqtSignal(str)
    finishSiganl = QtCore.pyqtSignal(dict)  # files dict, index data
    def __init__(self, directory, whooshObj, parent= None) -> None:
        super().__init__(parent=parent)
        self.directory = directory
        self.whoosh = whooshObj
        self.isProccesing = False
        self.setWindowTitle('فهرست گیری')
        self.setFixedSize(650, 575)
        font = QtGui.QFont(GENERAL_FONT_NAME, 10)
        self.setFont(font)
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.verticalLayout.setContentsMargins(10, 20, 10, 10)
        self.verticalLayout.setSpacing(20)

        self.folderNameLabel = QtWidgets.QLabel(self)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.folderNameLabel.sizePolicy().hasHeightForWidth())
        self.folderNameLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont(GENERAL_FONT_NAME, 15)
        self.folderNameLabel.setFont(font)
        self.folderNameLabel.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.folderNameLabel.setText(self.directory.split('/')[-1])
        self.verticalLayout.addWidget(self.folderNameLabel)

        self.frame = QtWidgets.QFrame(self)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)

        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)

        self.label = QtWidgets.QLabel(self.frame)
        self.label.setFont(QtGui.QFont(GENERAL_FONT_NAME, 12))
        self.label.setText("فایل ها")
        self.verticalLayout_2.addWidget(self.label)

        self.filesList = QtWidgets.QListWidget(self.frame)
        self.filesList.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.filesList.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.filesList.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.filesList.setMovement(QtWidgets.QListView.Static)
        self.filesList.setFlow(QtWidgets.QListView.TopToBottom)
        self.filesList.setViewMode(QtWidgets.QListView.ListMode)
        self.filesList.setSortingEnabled(True)
        self.filesList.setSpacing(5)
        self.filesList.setFont(QtGui.QFont(GENERAL_FONT_NAME))
        self.filesList.setSortingEnabled(False)
        self.filesList.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.verticalLayout_2.addWidget(self.filesList)
        self.verticalLayout.addWidget(self.frame)
        
        self.frame_2 = QtWidgets.QFrame(self)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.frame_2)
        self.verticalLayout_3.setContentsMargins(0, 9, 0, 9)
        self.verticalLayout_3.setObjectName("verticalLayout_3")

        self.label_2 = QtWidgets.QLabel(self.frame_2)
        self.label_2.setFont(QtGui.QFont(GENERAL_FONT_NAME, 12))
        self.label_2.setText("خطاها")
        self.label_2.setObjectName("label_2")
        self.verticalLayout_3.addWidget(self.label_2)
        self.errorTable = QtWidgets.QTableWidget(self.frame_2)
        self.errorTable.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.errorTable.setFont(QtGui.QFont(GENERAL_FONT_NAME))
        self.errorTable.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.errorTable.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.errorTable.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.errorTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.errorTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.errorTable.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.errorTable.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.errorTable.setAlternatingRowColors(True)
        self.errorTable.setObjectName("errorTable")
        self.errorTable.setColumnCount(3)
        item = QtWidgets.QTableWidgetItem("سند")
        self.errorTable.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem("خطا")
        self.errorTable.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem("مسیر")
        self.errorTable.setHorizontalHeaderItem(2, item)

        self.errorTable.horizontalHeader().setCascadingSectionResizes(True)
        self.errorTable.horizontalHeader().setDefaultSectionSize(200)
        self.errorTable.horizontalHeader().setHighlightSections(False)
        self.errorTable.horizontalHeader().setMinimumSectionSize(50)
        self.errorTable.horizontalHeader().setStretchLastSection(True)
        self.errorTable.verticalHeader().setVisible(False)
        self.errorTable.verticalHeader().setStretchLastSection(False)
        self.verticalLayout_3.addWidget(self.errorTable)
        self.verticalLayout.addWidget(self.frame_2)

        self.finishLabel = QtWidgets.QLabel(self)
        self.finishLabel.setObjectName("finishLabel")
        self.finishLabel.setFont(QtGui.QFont(GENERAL_FONT_NAME, 12))
        self.finishLabel.setStyleSheet("color: %s" %(BUTTON_BACKGROUND_COLOR))
        self.finishLabel.setText("فهرست گیری تمام شد")
        self.finishLabel.hide()
        self.verticalLayout.addWidget(self.finishLabel)

        self.startButton = QtWidgets.QPushButton(self)
        self.startButton.setMinimumSize(QtCore.QSize(0, 35))
        self.startButton.setFont(QtGui.QFont(GENERAL_FONT_NAME, 12))
        self.startButton.setObjectName("startButton")
        self.startButton.setStyleSheet("""*{border:none;
                                            border-radius:12px;
                                            background-color: %s;
                                            color: #FFFFFF;
                                            } 
                                            *:hover{background-color: %s;}
                                            """ %(BUTTON_BACKGROUND_COLOR, BUTTON_HOVER_BACKGROUND_COLOR))
        self.startButton.setText("فهرست گیری")
        self.startButton.clicked.connect(self.startOperation)
        self.verticalLayout.addWidget(self.startButton)

    def startOperation(self):
        self.isProccesing = True
        self.startButton.setDisabled(True)
        self.whoosh.makeIndexDir(self.directory)
        self.whoosh.createIndexer()

        self.filesCounter = 0
        
        self.all_files = {'name': self.directory.split('/')[-1], 'path': self.directory, 'data':{}}
        self.all_files['data'][self.all_files['name']] = {' ': []}
        self.findFiles(self.directory, self.all_files['data'][self.all_files['name']])

        self.label.setText(f"فایل ها ({self.filesCounter})")
            
        self.startButton.hide()
        self.finishLabel.show()
        if self.isProccesing:
            self.whoosh.saveFileInfo(self.all_files)
            self.finishSiganl.emit(self.all_files)
        self.isProccesing = False
        # self.directorySignal.emit(self.directory)

    def addError(self, fileName, path, errorText):
        newRow = self.errorTable.rowCount()
        self.errorTable.insertRow(newRow)
        self.errorTable.setItem(newRow, 0, QtWidgets.QTableWidgetItem(fileName))
        self.errorTable.setItem(newRow, 1, QtWidgets.QTableWidgetItem(errorText))
        self.errorTable.setItem(newRow, 2, QtWidgets.QTableWidgetItem(path))
        self.errorTable.resizeRowsToContents()
        self.errorTable.scrollToBottom()
        app.processEvents()


    def findFiles(self, folderPath, savedict):
        for file in os.listdir(folderPath):
            if not self.isProccesing:
                break
            if os.path.isfile(folderPath + '/' + file):
                item = QtWidgets.QListWidgetItem(file)
                try:
                    self.whoosh.indexDocument(self.whoosh.indexer, folderPath + '/' + file)
                    self.filesList.addItem(item)
                    savedict[' '].append(folderPath + '/' + file)
                    self.filesList.scrollToBottom()
                    self.filesCounter += 1
                    app.processEvents()
                except Exception as e:
                    self.addError(file, folderPath + '/' + file, e)
            else:
                savedict[file] = {' ':[]}
                self.findFiles(folderPath + '/' + file,  savedict[file])
    
    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        print('MIG MIG '*10)
        if self.isProccesing:
            self.isProccesing = False
            self.whoosh.deleteIndexer(self.directory)
            print(f'{self.directory} not added !')
        return super().closeEvent(event)



class localSearchDialog(QtWidgets.QDialog):
    clearScene = QtCore.pyqtSignal()
    def __init__(self, parent=None) -> None:
        super().__init__(parent=parent)

        self.currentHighlightItemNumber = -1
        self.allLocalSearchItems = []
        self.word = None

        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setFixedSize(400, 46)
        self.setStyleSheet("""*{
                                border: none;
                                background-color: rgb(255, 255, 255);
                                font: 10pt \"B Homa\";
                            }
                            #inputLine{
                                margin-right:10px; 
                            }
                            #MainFrame{
                                border: 1px solid #000000;
                            }
                            #closeButton{
                                padding: 5px;
                                border-left: 1px solid #000000;
                                background-color: %s;
                            }
                            #closeButton:hover, #previousButton:hover, #nextButton:hover{
                                background-color: %s;
                            }
                            #nextButton, #previousButton{
                                margin: 5px;
                                border-radius: 5px
                            }
                            #number{
                                border-left: 1px solid #000000;
                                padding-left: 7px;
                                padding-right: 7px;
                            }""" %(BUTTON_BACKGROUND_COLOR, BUTTON_HOVER_BACKGROUND_COLOR))
        self.horizontalLayout = QtWidgets.QHBoxLayout(self)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.MainFrame = QtWidgets.QFrame(self)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.MainFrame.sizePolicy().hasHeightForWidth())
        self.MainFrame.setSizePolicy(sizePolicy)
        self.MainFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.MainFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.MainFrame.setObjectName("MainFrame")
        self.MainFrame.setGraphicsEffect(QtWidgets.QGraphicsDropShadowEffect(self))
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.MainFrame)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.previousButton = QtWidgets.QPushButton(self.MainFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.previousButton.sizePolicy().hasHeightForWidth())
        self.previousButton.setSizePolicy(sizePolicy)
        self.previousButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("image/upArrow.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.previousButton.setIcon(icon)
        self.previousButton.setIconSize(QtCore.QSize(24, 24))
        self.previousButton.setObjectName("previousButton")
        self.previousButton.setDisabled(True)
        self.horizontalLayout_2.addWidget(self.previousButton)
        self.nextButton = QtWidgets.QPushButton(self.MainFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.nextButton.sizePolicy().hasHeightForWidth())
        self.nextButton.setSizePolicy(sizePolicy)
        self.nextButton.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("image/downArrow.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.nextButton.setIcon(icon1)
        self.nextButton.setIconSize(QtCore.QSize(24, 24))
        self.nextButton.setObjectName("nextButton")
        self.nextButton.setDisabled(True)
        self.horizontalLayout_2.addWidget(self.nextButton)
        self.number = QtWidgets.QLabel(self.MainFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.number.sizePolicy().hasHeightForWidth())
        self.number.setSizePolicy(sizePolicy)
        self.number.setObjectName("number")
        self.number.setText("0/0")
        # self.number.hide()
        self.horizontalLayout_2.addWidget(self.number)
        self.inputLine = QtWidgets.QLineEdit(self.MainFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.inputLine.sizePolicy().hasHeightForWidth())
        self.inputLine.setSizePolicy(sizePolicy)
        self.inputLine.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.inputLine.setObjectName("inputLine")
        self.inputLine.setPlaceholderText("واژه مورد نظر را وارد کنید")
        # self.inputLine.returnPressed.connect(self.F_Find)
        self.horizontalLayout_2.addWidget(self.inputLine)
        self.closeButton = QtWidgets.QPushButton(self.MainFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.closeButton.sizePolicy().hasHeightForWidth())
        self.closeButton.setSizePolicy(sizePolicy)
        self.closeButton.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("image/whiteClose.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.closeButton.setIcon(icon2)
        self.closeButton.setIconSize(QtCore.QSize(24, 24))
        self.closeButton.setObjectName("closeButton")
        self.closeButton.clicked.connect(self.close)
        self.horizontalLayout_2.addWidget(self.closeButton)
        self.horizontalLayout.addWidget(self.MainFrame)


    def close(self) -> bool:
        self.clearScene.emit()
        if self.inputLine.text():
            self.inputLine.setText("")
            self.number.setText('0/0')
        else:
            return super().close()






class simpleOptionWidget(customButton):
    def __init__(self, ID, text, parent=None) -> None:
        super().__init__(ID, parent=parent)
        self.label = QtWidgets.QLabel(self.topContainer)
        self.label.setText(text)
        
        font = QtGui.QFont(OPTION_FONT[0], OPTION_FONT[1])
        self.label.setFont(font)

        self.horizontalLayout_2.addWidget(self.label)
        self.horizontalLayout_2.setContentsMargins(0, 0, 20, 0)

